package com.ikrisztian.marketplace;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ikrisztian.marketplace.dto.in.EditUserDto;
import com.ikrisztian.marketplace.dto.out.UserDto;
import com.ikrisztian.marketplace.repository.UserRepository;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class UserTests {
  @Autowired private MockMvc mockMvc;

  @Autowired private ObjectMapper objectMapper;

  @Autowired private UserRepository userRepository;
  private EditUserDto editUserDto;

  @Before
  public void setUp() {
    editUserDto = new EditUserDto();
    editUserDto.setEmail("email@host.com");
    editUserDto.setFirstName("Emil");
    editUserDto.setLastName("User");
    editUserDto.setPassword("PaSsWoRd");
  }

  @After
  public void tearDown() {
    userRepository.deleteAll();
  }

  @Test
  public void shouldBeAbleToCreateUser() throws Exception {
    // WHEN
    MockHttpServletResponse response = createUser(editUserDto);

    // THEN
    UserDto userDto = new UserDto();
    userDto.setEmail("email@host.com");
    userDto.setFirstName("Emil");
    userDto.setLastName("User");
    userDto.setRating(0);

    UserDto actual = objectMapper.readValue(response.getContentAsString(), UserDto.class);

    assertThat(response.getStatus()).isEqualTo(HttpStatus.CREATED.value());
    assertThat(actual).isEqualToIgnoringGivenFields(userDto, "id");
    assertThat(userRepository.findAll()).hasSize(1);
  }

  private MockHttpServletResponse createUser(EditUserDto editUserDto) throws Exception {
    String json = objectMapper.writeValueAsString(editUserDto);
    return mockMvc
        .perform(post("/api/users").contentType(MediaType.APPLICATION_JSON).content(json))
        .andReturn()
        .getResponse();
  }

  @Test
  public void userDtoShouldBeValid() throws Exception {
    // GIVEN
    EditUserDto editUserDto = new EditUserDto();
    editUserDto.setEmail(null);
    editUserDto.setFirstName("Emil");
    editUserDto.setLastName("User");
    editUserDto.setPassword("PaSsWoRd");

    // WHEN
    MockHttpServletResponse response = createUser(editUserDto);

    // THEN
    assertThat(response.getStatus()).isEqualTo(HttpStatus.BAD_REQUEST.value());
    assertThat(userRepository.findAll()).isEmpty();
  }

  @Test
  public void deleteShouldWork() throws Exception {
    // GIVEN
    createUser(editUserDto);
    Long id = userRepository.findAll().get(0).getId();

    // WHEN
    MockHttpServletResponse deleteResponse = mockMvc.perform(delete("/api/users/" + id)).andReturn().getResponse();

    // THEN
    assertThat(deleteResponse.getStatus()).isEqualTo(HttpStatus.NO_CONTENT.value());
    assertThat(userRepository.findAll()).isEmpty();
  }
}
