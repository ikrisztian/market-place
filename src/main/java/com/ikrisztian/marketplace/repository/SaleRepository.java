package com.ikrisztian.marketplace.repository;

import com.ikrisztian.marketplace.entity.Sale;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SaleRepository extends JpaRepository<Sale, Long> {

    List<Sale> findAllByProduct_Id(Long id);
}
