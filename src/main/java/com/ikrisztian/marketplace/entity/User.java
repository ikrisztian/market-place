package com.ikrisztian.marketplace.entity;

import org.springframework.data.annotation.Transient;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import java.util.List;
import java.util.Objects;

@Entity
public class User {
  @Id @GeneratedValue private Long id;

  @Email
  @NotBlank
  @Column(unique = true)
  private String email;

  @NotBlank private String firstName;
  @NotBlank private String lastName;
  @Transient private double numberOfRates;
  @NotBlank private String password;
  @Transient private double sumOfRates;

  @OneToMany(mappedBy = "seller")
  private List<Product> products;

  @OneToMany(mappedBy = "customer")
  private List<Sale> salesAsCustomer;

  @OneToMany(mappedBy = "seller")
  private List<Sale> salesAsSeller;


  public List<Sale> getSalesAsCustomer() {
    return salesAsCustomer;
  }

  public void setSalesAsCustomer(List<Sale> salesAsCustomer) {
    this.salesAsCustomer = salesAsCustomer;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public List<Product> getProducts() {
    return products;
  }

  public void setProducts(List<Product> products) {
    this.products = products;
  }

  public double getNumberOfRates() {
    return numberOfRates;
  }

  public void setNumberOfRates(double numberOfRates) {
    this.numberOfRates = numberOfRates;
  }

  public double getSumOfRates() {
    return sumOfRates;
  }

  public void setSumOfRates(double sumOfRates) {
    this.sumOfRates = sumOfRates;
  }

  public List<Sale> getSalesAsSeller() {
    return salesAsSeller;
  }

  public Long getSoldValue() {
    return salesAsSeller.stream().mapToLong(Sale::getActualProductPrice).sum();
  }

  public void setSalesAsSeller(List<Sale> salesAsSeller) {
    this.salesAsSeller = salesAsSeller;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    User user = (User) o;
    return Double.compare(user.numberOfRates, numberOfRates) == 0
        && Double.compare(user.sumOfRates, sumOfRates) == 0
        && Objects.equals(id, user.id)
        && Objects.equals(firstName, user.firstName)
        && Objects.equals(lastName, user.lastName)
        && Objects.equals(email, user.email)
        && Objects.equals(password, user.password)
        && Objects.equals(products, user.products)
        && Objects.equals(salesAsCustomer, user.salesAsCustomer);
  }

  @Override
  public int hashCode() {
    return Objects.hash(
        id,
        firstName,
        lastName,
        email,
        password,
        products,
        salesAsCustomer,
        numberOfRates,
        sumOfRates);
  }

  @Override
  public String toString() {
    return "User{"
        + "id="
        + id
        + ", firstName='"
        + firstName
        + '\''
        + ", lastName='"
        + lastName
        + '\''
        + ", email='"
        + email
        + '\''
        + ", password='"
        + password
        + '\''
        + ", products="
        + products
        + ", salesAsCustomer="
        + salesAsCustomer
        + ", numberOfRates="
        + numberOfRates
        + ", sumOfRates="
        + sumOfRates
        + '}';
  }
}
