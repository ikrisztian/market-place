package com.ikrisztian.marketplace.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import java.util.Objects;

@Entity
public class Rating {
    @Id @GeneratedValue
    private Long id;
    @ManyToOne
    private User ratedBy;
    @ManyToOne private User ratedUser;
    private Integer rating;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getRatedBy() {
        return ratedBy;
    }

    public void setRatedBy(User ratedBy) {
        this.ratedBy = ratedBy;
    }

    public User getRatedUser() {
        return ratedUser;
    }

    public void setRatedUser(User ratedUser) {
        this.ratedUser = ratedUser;
    }

    public Integer getRating() {
        return rating;
    }

    public void setRating(Integer rating) {
        this.rating = rating;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Rating rating1 = (Rating) o;
        return Objects.equals(id, rating1.id) &&
                Objects.equals(ratedBy.getId(), rating1.ratedBy.getId()) &&
                Objects.equals(ratedUser.getId(), rating1.ratedUser.getId()) &&
                Objects.equals(rating, rating1.rating);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, ratedBy.getId(), ratedUser.getId(), rating);
    }

    @Override
    public String toString() {
        return "Rating{" +
                "id=" + id +
                ", ratedBy=" + ratedBy.getEmail() +
                ", ratedUser=" + ratedUser.getEmail() +
                ", rating=" + rating +
                '}';
    }
}
