package com.ikrisztian.marketplace.entity;

import javax.persistence.*;
import java.util.Objects;

@Entity
public class Sale {
  @Id @GeneratedValue private Long id;
  private Long actualProductPrice;
  @ManyToOne private User customer;
  @ManyToOne private Product product;
  @ManyToOne private User seller;
//  @OneToOne
//  private String category;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public User getCustomer() {
    return customer;
  }

  public void setCustomer(User customer) {
    this.customer = customer;
  }

  public Product getProduct() {
    return product;
  }

  public void setProduct(Product product) {
    this.product = product;
  }

  public Long getActualProductPrice() {
    return actualProductPrice;
  }

  public void setActualProductPrice(Long actualProductPrice) {
    this.actualProductPrice = actualProductPrice;
  }

  public User getSeller() {
    return seller;
  }

  public void setSeller(User seller) {
    this.seller = seller;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    Sale sale = (Sale) o;
    return Objects.equals(id, sale.id)
        && Objects.equals(customer, sale.customer)
        && Objects.equals(product, sale.product)
        && Objects.equals(actualProductPrice, sale.actualProductPrice)
        && Objects.equals(seller, sale.seller);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, customer, product, actualProductPrice, seller);
  }

  @Override
  public String toString() {
    return "Sale{"
        + "id="
        + id
        + ", customer="
        + customer
        + ", product="
        + product
        + ", actualProductPrice="
        + actualProductPrice
        + ", seller="
        + seller
        + '}';
  }
}
