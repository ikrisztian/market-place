package com.ikrisztian.marketplace.entity;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Entity
public class Product {
  @Id @GeneratedValue private Long id;
  private String category;
  private String description;
  private String name;
  private Integer numberOfQueries;
  private Long price;
  private Integer stock;
  @ManyToOne private User seller;

  @OneToMany(
      mappedBy = "product",
      cascade = {CascadeType.PERSIST})
  private List<Sale> sales;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public Long getPrice() {
    return price;
  }

  public void setPrice(Long price) {
    this.price = price;
  }

  public String getCategory() {
    return category;
  }

  public void setCategory(String category) {
    this.category = category;
  }

  public User getSeller() {
    return seller;
  }

  public void setSeller(User seller) {
    this.seller = seller;
  }

  public Integer getStock() {
    return stock;
  }

  public void setStock(Integer stock) {
    this.stock = stock;
  }

  public Integer getNumberOfQueries() {
    return numberOfQueries;
  }

  public void setNumberOfQueries(Integer numberOfQueries) {
    this.numberOfQueries = numberOfQueries;
  }

  public List<Sale> getSales() {
    return sales;
  }

  public void setSales(List<Sale> sales) {
    this.sales = sales;
  }

  //  public Integer getSalesUnit() {
  //    return salesUnit;
  //  }
  //
  //  public void setSalesUnit(Integer salesUnit) {
  //    this.salesUnit = salesUnit;
  //  }

  @Override
  public String toString() {
    return "Product{"
        + "id="
        + id
        + ", name='"
        + name
        + '\''
        + ", description='"
        + description
        + '\''
        + ", price="
        + price
        + ", category='"
        + category
        + '\''
        + ", seller='"
        + seller.getEmail()
        + '\''
        + ", stock="
        + stock
        + ", numberOfQueries="
        + numberOfQueries
        + ", sales="
        + sales.stream().map(Sale::getId).collect(Collectors.toList())
        + '}';
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    Product product = (Product) o;
    return Objects.equals(id, product.id)
        && Objects.equals(name, product.name)
        && Objects.equals(description, product.description)
        && Objects.equals(price, product.price)
        && Objects.equals(category, product.category)
        && Objects.equals(seller, product.seller)
        && Objects.equals(stock, product.stock)
        && Objects.equals(numberOfQueries, product.numberOfQueries)
        && Objects.equals(sales, product.sales);
  }

  @Override
  public int hashCode() {
    return Objects.hash(
        id, name, description, price, category, seller, stock, numberOfQueries, sales);
  }
}
