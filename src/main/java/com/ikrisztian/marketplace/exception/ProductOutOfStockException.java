package com.ikrisztian.marketplace.exception;

public class ProductOutOfStockException extends MarketPlaceException {
  public ProductOutOfStockException(Long id) {
    super(4, "Product out of stock with id: " + id);
  }
}
