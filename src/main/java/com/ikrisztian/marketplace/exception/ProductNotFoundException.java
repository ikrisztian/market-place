package com.ikrisztian.marketplace.exception;

public class ProductNotFoundException extends MarketPlaceException{
  public ProductNotFoundException(Long id) {
    super(3, "Product not found with id: " + id);
  }
}
