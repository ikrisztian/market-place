package com.ikrisztian.marketplace.exception;

public class IllegalRatingValueException extends MarketPlaceException {
    public IllegalRatingValueException(String description) {
        super(5, description);
    }
}
