package com.ikrisztian.marketplace.exception;

public abstract class MarketPlaceException extends RuntimeException {
  protected final int errorCode;

  public MarketPlaceException(int errorCode, String description) {
    super(description);
    this.errorCode = errorCode;
  }

    public int getErrorCode() {
        return errorCode;
    }
}
