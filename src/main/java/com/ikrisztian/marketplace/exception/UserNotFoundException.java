package com.ikrisztian.marketplace.exception;

public class UserNotFoundException extends MarketPlaceException {
  public UserNotFoundException(Long id) {
    super(2, "User not found with id: " + id);
  }
}
