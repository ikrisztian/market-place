package com.ikrisztian.marketplace.service;

import com.ikrisztian.marketplace.entity.Product;
import com.ikrisztian.marketplace.entity.Sale;
import com.ikrisztian.marketplace.entity.User;
import com.ikrisztian.marketplace.repository.SaleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SaleService {

  private SaleRepository saleRepository;

  @Autowired
  public SaleService(SaleRepository saleRepository) {
    this.saleRepository = saleRepository;
  }

  public void addSales(User user, Product product) {
    Sale sale = new Sale();
    sale.setCustomer(user);
    sale.setProduct(product);
    sale.setActualProductPrice(product.getPrice());
    saleRepository.save(sale);
  }

  public List<Sale> getSalesForProduct(Long id) {
    return saleRepository.findAllByProduct_Id(id);
  }

}
