package com.ikrisztian.marketplace.service;

import com.ikrisztian.marketplace.dto.in.EditProductDto;
import com.ikrisztian.marketplace.dto.out.ProductCategorySalesStatsDto;
import com.ikrisztian.marketplace.dto.out.ProductDto;
import com.ikrisztian.marketplace.dto.out.ProductPopularityDto;
import com.ikrisztian.marketplace.dto.out.ProductSalesStatsDto;
import com.ikrisztian.marketplace.entity.Product;
import com.ikrisztian.marketplace.entity.Sale;
import com.ikrisztian.marketplace.exception.ProductNotFoundException;
import com.ikrisztian.marketplace.exception.ProductOutOfStockException;
import com.ikrisztian.marketplace.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class ProductService {
  private ProductRepository productRepository;
  private SaleService saleService;

  @Autowired
  public ProductService(ProductRepository productRepository, SaleService saleService) {
    this.productRepository = productRepository;
    this.saleService = saleService;
  }

  public ProductDto save(EditProductDto editProductDto) {
    Product product = transformEditDtoToProduct(editProductDto);
    Product productInDatabase = productRepository.save(product);
    return transformToProductDto(productInDatabase);
  }

  private Product transformEditDtoToProduct(EditProductDto editProductDto) {
    Product product = new Product();
    product.setCategory(editProductDto.getCategory());
    product.setDescription(editProductDto.getDescription());
    product.setName(editProductDto.getName());
    product.setPrice(editProductDto.getPrice());
    product.setNumberOfQueries(0);
    product.setSeller(null);
    product.setStock(editProductDto.getStock());

    return product;
  }

  private static ProductDto transformToProductDto(Product product) {
    ProductDto productDto = new ProductDto();
    productDto.setCategory(product.getCategory());
    productDto.setDescription(product.getDescription());
    productDto.setName(product.getName());
    productDto.setPrice(product.getPrice());
    productDto.setId(product.getId());
    productDto.setStock(product.getStock());

    return productDto;
  }

  public List<ProductDto> listProducts() {
    return productRepository
        .findAll()
        .stream()
        .map(ProductService::transformToProductDto)
        .collect(Collectors.toList());
  }

  public ProductDto listProductById(Long id) {
    Product product = getProduct(id);
    product.setNumberOfQueries(product.getNumberOfQueries() + 1);
    productRepository.save(product);
    return transformToProductDto(product);
  }

  private Product getProduct(Long id) {
    return productRepository.findById(id).orElseThrow(() -> new ProductNotFoundException(id));
  }

  public ProductDto updateProduct(Long id, EditProductDto editProductDto) {
    validateId(id);
    Product product = transformEditDtoToProduct(editProductDto);
    product.setId(id);
    Product productInDatabase = productRepository.save(product);
    return transformToProductDto(productInDatabase);
  }

  private void validateId(Long id) {
    if (!productRepository.findById(id).isPresent()) throw new ProductNotFoundException(id);
  }

  public void delete(Long id) {
    validateId(id);
    productRepository.deleteById(id);
  }

  @Transactional
  public synchronized ProductDto buy(Long id) {
    Product lockedProduct = getProductWithExistingStock(id);
    lockedProduct.setStock(lockedProduct.getStock() - 1);
    Product updatedProduct = productRepository.save(lockedProduct);
    saleService.addSales(null, updatedProduct);
    updatedProduct = productRepository.save(updatedProduct);
    return transformToProductDto(updatedProduct);
  }

  private Product getProductWithExistingStock(Long id) {
    Product product = getProduct(id);
    if (product.getStock() <= 0) {
      throw new ProductOutOfStockException(id);
    }
    return product;
  }

  public ProductPopularityDto popularityForId(Long id) {
    Product product = getProduct(id);
    ProductPopularityDto dto = new ProductPopularityDto();
    dto.setProductId(id);
    dto.setNumberOfQueries(product.getNumberOfQueries());
    return dto;
  }

  public List<ProductPopularityDto> popularityTop5() {
    return productRepository
        .findAllByOrderByNumberOfQueriesDesc()
        .stream()
        .map(this::transformProductToProductPopularityDto)
        .limit(5)
        .collect(Collectors.toList());
  }

  private ProductPopularityDto transformProductToProductPopularityDto(Product product) {
    ProductPopularityDto dto = new ProductPopularityDto();
    dto.setProductId(product.getId());
    dto.setNumberOfQueries(product.getNumberOfQueries());
    return dto;
  }

  public ProductSalesStatsDto saleStats(Long id) {
    Product p = getProduct(id);
    return transformProductToProductSalesStatsDto(p);
  }

  private ProductSalesStatsDto transformProductToProductSalesStatsDto(Product p) {
    List<Sale> sales = saleService.getSalesForProduct(p.getId());

    Long price = p.getPrice();
    int salesNumber = sales.size();

    ProductSalesStatsDto statsDto = new ProductSalesStatsDto();
    statsDto.setId(p.getId());
    statsDto.setPrice(price);
    statsDto.setSoldUnits(salesNumber);
    statsDto.setAggregatedValue(salesNumber * price);
    return statsDto;
  }

  public List<ProductSalesStatsDto> salesStats(String order) {
    List<Product> all =
        "desc".equalsIgnoreCase(order)
            ? productRepository.findAllByOrderByPriceDesc()
            : productRepository.findAllByOrderByPriceAsc();
    return all.stream()
        .map(this::transformProductToProductSalesStatsDto)
        .collect(Collectors.toList());
  }

  public List<String> getProductCategories() {
    return productRepository
        .findAll()
        .stream()
        .map(this::transformProductToProductCategory)
        .distinct()
        .collect(Collectors.toList());
  }

  private String transformProductToProductCategory(Product product) {
    return product.getCategory();
  }

  public List<ProductCategorySalesStatsDto> getProductCategorySalesStats() {
    Map<String, Long> map =
        productRepository
            .findAll()
            .stream()
            .collect(
                Collectors.toMap(
                    Product::getCategory,
                    product -> product.getPrice() * product.getSales().size(),
                    (oldValue, newValue) -> oldValue + newValue));
    return map.entrySet()
        .stream()
        .map(ProductService::transformToProductCategorySalesStatsDto)
        .collect(Collectors.toList());
  }

  private static ProductCategorySalesStatsDto transformToProductCategorySalesStatsDto(
      Map.Entry<String, Long> stringLongEntry) {
    ProductCategorySalesStatsDto dto = new ProductCategorySalesStatsDto();
    dto.setCategory(stringLongEntry.getKey());
    dto.setSumOfProductPriceByCategory(stringLongEntry.getValue());
    return dto;
  }
}
