package com.ikrisztian.marketplace.service;

import com.ikrisztian.marketplace.dto.in.EditUserDto;
import com.ikrisztian.marketplace.dto.out.UserDto;
import com.ikrisztian.marketplace.dto.out.UserRatingDto;
import com.ikrisztian.marketplace.dto.out.UserSalesStatsDto;
import com.ikrisztian.marketplace.entity.User;
import com.ikrisztian.marketplace.exception.IllegalRatingValueException;
import com.ikrisztian.marketplace.exception.UserNotFoundException;
import com.ikrisztian.marketplace.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.Collections.reverseOrder;
import static java.util.Comparator.comparingDouble;

@Service
public class UserService {
  private UserRepository repository;

  @Autowired
  public UserService(UserRepository repository) {
    this.repository = repository;
  }

  public UserDto save(EditUserDto editUserDto) {
    User user = transformEditDtoToUser(editUserDto);
    User userInDatabase = repository.save(user);
    return transformToUserDto(userInDatabase);
  }

  private User transformEditDtoToUser(EditUserDto editUserDto) {
    User user = new User();
    user.setFirstName(editUserDto.getFirstName());
    user.setLastName(editUserDto.getLastName());
    user.setEmail(editUserDto.getEmail());
    user.setPassword(editUserDto.getPassword());
    return user;
  }

  private static UserDto transformToUserDto(User userInDatabase) {
    UserDto userDto = new UserDto();
    userDto.setId(userInDatabase.getId());
    userDto.setFirstName(userInDatabase.getFirstName());
    userDto.setLastName(userInDatabase.getLastName());
    userDto.setEmail(userInDatabase.getEmail());
    userDto.setRating(userInDatabase.getSumOfRates() / userInDatabase.getNumberOfRates());
    return userDto;
  }

  public List<UserDto> listUsers() {
    return repository
        .findAll()
        .stream()
        .map(UserService::transformToUserDto)
        .collect(Collectors.toList());
  }

  public UserDto listUserById(Long id) {
    User user = getUser(id);
    return transformToUserDto(user);
  }

  private User getUser(Long id) {
    return repository.findById(id).orElseThrow(() -> new UserNotFoundException(id));
  }

  public UserDto updateUser(Long id, EditUserDto editUserDto) {
    validateId(id);
    User user = transformEditDtoToUser(editUserDto);
    user.setId(id);
    User userInDatabase = repository.save(user);
    return transformToUserDto(userInDatabase);
  }

  private void validateId(Long id) {
    if (!repository.findById(id).isPresent()) throw new UserNotFoundException(id);
  }

  public void delete(Long id) {
    validateId(id);
    repository.deleteById(id);
  }

  public UserRatingDto rateUser(Long id, Integer value) {
    validateRating(value);
    User user = getUser(id);
    user.setNumberOfRates(user.getNumberOfRates() + 1);
    user.setSumOfRates(user.getSumOfRates() + value);
    User saved = repository.save(user);
    return transformUserToUserRatingDto(saved);
  }

  private UserRatingDto transformUserToUserRatingDto(User user) {
    double avgRating;
    if (user.getNumberOfRates() == 0) {
      avgRating = 0;
    } else {
      avgRating = user.getSumOfRates() / user.getNumberOfRates();
    }
    UserRatingDto userRatingDto = new UserRatingDto();
    userRatingDto.setRating(avgRating);
    userRatingDto.setUserId(user.getId());
    return userRatingDto;
  }

  private void validateRating(Integer value) {
    if (value < 1 || value > 5) {
      throw new IllegalRatingValueException("Rating must be between 1 and 5, but was: " + value);
    }
  }

  public List<UserSalesStatsDto> salesUnitStats(String order) {
    Stream<User> stream = repository.findAll().stream();
    if ("asc".equalsIgnoreCase(order)) {
      return stream
          .sorted(salesUnitComparator())
          .map(this::transformToSaleStatsDto)
          .collect(Collectors.toList());
    }
    return stream
        .sorted(reverseOrder(salesUnitComparator()))
        .map(this::transformToSaleStatsDto)
        .collect(Collectors.toList());
  }

  private static Comparator<User> salesUnitComparator() {
    return Comparator.comparingInt(o -> o.getSalesAsSeller().size());
  }

  public List<UserSalesStatsDto> salesValueStats(String order) {
    Stream<User> stream = repository.findAll().stream();
    if ("asc".equalsIgnoreCase(order)) {
      return stream
          .map(this::transformToSaleStatsDto)
          .sorted(salesValueComparator())
          .collect(Collectors.toList());
    }
    return stream
        .map(this::transformToSaleStatsDto)
        .sorted(reverseOrder(salesValueComparator()))
        .collect(Collectors.toList());
  }

  private static Comparator<UserSalesStatsDto> salesValueComparator() {
    return Comparator.comparingInt(o -> Math.toIntExact(o.getSoldValue()));
  }

  private UserSalesStatsDto transformToSaleStatsDto(User user) {
    UserSalesStatsDto dto = new UserSalesStatsDto();
    dto.setId(user.getId());
    dto.setSoldUnits(user.getSalesAsSeller().size());
    dto.setSoldValue(user.getSoldValue());
    return dto;
  }

  public List<UserRatingDto> listUserByRating(String order) {
    Stream<User> stream = repository.findAll().stream();
    if ("asc".equalsIgnoreCase(order)) {
      return stream
          .map(this::transformUserToUserRatingDto)
          .sorted(comparingDouble(UserRatingDto::getRating))
          .collect(Collectors.toList());
    }
    return stream
        .map(this::transformUserToUserRatingDto)
        .sorted(reverseOrder(comparingDouble(UserRatingDto::getRating)))
        .collect(Collectors.toList());
  }
}
