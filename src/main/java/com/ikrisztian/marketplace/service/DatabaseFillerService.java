package com.ikrisztian.marketplace.service;

import com.ikrisztian.marketplace.entity.Product;
import com.ikrisztian.marketplace.entity.Sale;
import com.ikrisztian.marketplace.entity.User;
import com.ikrisztian.marketplace.repository.ProductRepository;
import com.ikrisztian.marketplace.repository.SaleRepository;
import com.ikrisztian.marketplace.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

@Service
public class DatabaseFillerService {

  private ProductRepository productRepository;
  private UserRepository userRepository;
  private SaleRepository saleRepository;

  @Autowired
  public DatabaseFillerService(
      ProductRepository productRepository,
      UserRepository userRepository,
      SaleRepository saleRepository) {
    this.productRepository = productRepository;
    this.userRepository = userRepository;
    this.saleRepository = saleRepository;
  }

  @PostConstruct
  public void createProduct() {

    User user = new User();
    user.setPassword("asd");
    user.setLastName("lst");
    user.setFirstName("Gabor");
    user.setEmail("as@a.c");
    User gabor = userRepository.save(user);

    User user2 = new User();
    user2.setEmail("user@net.com");
    user2.setFirstName("Lilla");
    user2.setLastName("last");
    user2.setPassword("a");
    User lilla = userRepository.save(user2);

    User user3 = new User();
    user3.setEmail("new@gmail.com");
    user3.setFirstName("Tamas");
    user3.setLastName("Odd");
    user3.setPassword("b");
    User tamas = userRepository.save(user3);

    User user4 = new User();
    user4.setEmail("Four@gmail.com");
    user4.setFirstName("Eszter");
    user4.setLastName("asgckj");
    user4.setPassword("pass");
    User eszter = userRepository.save(user4);

    User user5 = new User();
    user5.setEmail("Five@er.com");
    user5.setFirstName("Pali");
    user5.setLastName("alkkhijnsc");
    user5.setPassword("pakss76");
    User pali = userRepository.save(user5);

    User user6 = new User();
    user6.setEmail("Six@er.com");
    user6.setFirstName("Erzsi");
    user6.setLastName("alkkjnsc");
    user6.setPassword("passy76");
    User erzsi = userRepository.save(user6);

    User user7 = new User();
    user7.setEmail("Seven@er.com");
    user7.setFirstName("Robi");
    user7.setLastName("alkkjnsc");
    user7.setPassword("pas76");
    User robi = userRepository.save(user7);

    Product productElectronics = new Product();
    productElectronics.setCategory("Electronics");
    productElectronics.setDescription("Printer");
    productElectronics.setName("HP");
    productElectronics.setNumberOfQueries(0);
    productElectronics.setPrice(500L);
    productElectronics.setStock(2);
    Product printer = productRepository.save(productElectronics);

    Product productElectronics2 = new Product();
    productElectronics2.setCategory("Electronics");
    productElectronics2.setDescription("Celly");
    productElectronics2.setName("Palm");
    productElectronics2.setNumberOfQueries(0);
    productElectronics2.setPrice(200L);
    productElectronics2.setStock(3);
    Product celly = productRepository.save(productElectronics2);

    Product productVehicle = new Product();
    productVehicle.setCategory("Vehicle");
    productVehicle.setDescription("Car");
    productVehicle.setName("Lada");
    productVehicle.setNumberOfQueries(0);
    productVehicle.setPrice(300L);
    productVehicle.setStock(3);
    Product lada = productRepository.save(productVehicle);

    Product productVehicle2 = new Product();
    productVehicle2.setCategory("Vehicle");
    productVehicle2.setDescription("Motorbike");
    productVehicle2.setName("Honda");
    productVehicle2.setNumberOfQueries(0);
    productVehicle2.setPrice(1200L);
    productVehicle2.setStock(5);
    Product honda = productRepository.save(productVehicle2);

    Product productClothes = new Product();
    productClothes.setCategory("Clothes");
    productClothes.setDescription("Hat");
    productClothes.setName("Cap");
    productClothes.setNumberOfQueries(0);
    productClothes.setPrice(10L);
    productClothes.setStock(5);
    Product cap = productRepository.save(productClothes);

    Product productClothes2 = new Product();
    productClothes2.setCategory("Clothes");
    productClothes2.setDescription("Trousers");
    productClothes2.setName("Jeans");
    productClothes2.setNumberOfQueries(0);
    productClothes2.setPrice(75L);
    productClothes2.setStock(50);
    Product jeans = productRepository.save(productClothes2);

    saveSale(gabor, lilla, celly);
    saveSale(gabor, lilla, lada);
    saveSale(lilla, gabor, celly);
    saveSale(lilla, gabor, printer);
    saveSale(eszter, gabor, lada);
    saveSale(lilla, tamas, celly);
    saveSale(tamas, gabor, jeans);
    saveSale(pali, gabor, jeans);
    saveSale(robi, eszter, cap);
    saveSale(eszter, lilla, printer);
    saveSale(gabor, lilla, honda);
    saveSale(pali, eszter, honda);
    saveSale(pali, lilla, honda);
    saveSale(tamas, eszter, jeans);
    saveSale(eszter, pali, celly);
    saveSale(pali, lilla, lada);
    saveSale(lilla, tamas, printer);
    saveSale(lilla, erzsi, printer);
    saveSale(lilla, tamas, lada);
    saveSale(gabor, lilla, cap);
    saveSale(gabor, pali, cap);
    saveSale(erzsi, robi, cap);
    saveSale(tamas, pali, honda);
    saveSale(lilla, gabor, cap);

  }

  private void saveSale(User customer, User seller, Product product) {
    Sale sale = new Sale();
    sale.setProduct(product);
    sale.setActualProductPrice(product.getPrice());
    sale.setCustomer(customer);
    sale.setSeller(seller);
    saleRepository.save(sale);
  }
}
