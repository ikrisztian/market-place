package com.ikrisztian.marketplace.dto.in;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import java.util.Objects;

public class EditProductDto {
  @NotBlank private String name;
  @NotBlank private String description;

  @Min(1)
  private Long price;

  @NotBlank private String category;
  private Integer stock;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public Long getPrice() {
    return price;
  }

  public void setPrice(Long price) {
    this.price = price;
  }

  public String getCategory() {
    return category;
  }

  public void setCategory(String category) {
    this.category = category;
  }

  public Integer getStock() {
    return stock;
  }

  public void setStock(Integer stock) {
    this.stock = stock;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    EditProductDto that = (EditProductDto) o;
    return Objects.equals(name, that.name)
        && Objects.equals(description, that.description)
        && Objects.equals(price, that.price)
        && Objects.equals(category, that.category)
        && Objects.equals(stock, that.stock);
  }

  @Override
  public int hashCode() {
    return Objects.hash(name, description, price, category, stock);
  }

  @Override
  public String toString() {
    return "EditProductDto{"
        + "name='"
        + name
        + '\''
        + ", description='"
        + description
        + '\''
        + ", price='"
        + price
        + '\''
        + ", category='"
        + category
        + '\''
        + ", stock='"
        + stock
        + '\''
        + '}';
  }
}
