package com.ikrisztian.marketplace.controller;

import com.ikrisztian.marketplace.dto.in.EditUserDto;
import com.ikrisztian.marketplace.dto.out.UserDto;
import com.ikrisztian.marketplace.dto.out.UserRatingDto;
import com.ikrisztian.marketplace.dto.out.UserSalesStatsDto;
import com.ikrisztian.marketplace.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@RestController
public class UserController {
  private UserService service;

  @Autowired
  public UserController(UserService service) {
    this.service = service;
  }

  @ResponseStatus(HttpStatus.CREATED)
  @PostMapping("/api/users")
  public UserDto save(@Valid @RequestBody EditUserDto editUserDto) {
    return service.save(editUserDto);
  }

  @GetMapping("/api/users")
  public List<UserDto> listUsers() {
    return service.listUsers();
  }

  @GetMapping("/api/users/{id}")
  public UserDto listUser(@PathVariable Long id) {
    return service.listUserById(id);
  }

  @PutMapping("/api/users/{id}")
  public UserDto updateUser(@PathVariable Long id, @Valid @RequestBody EditUserDto editUserDto) {
    return service.updateUser(id, editUserDto);
  }

  @DeleteMapping("/api/users/{id}")
  @ResponseStatus(HttpStatus.NO_CONTENT)
  public void deleteUser(@PathVariable Long id) {
    service.delete(id);
  }

  @PostMapping("/api/users/{id}/rate/{value}")
  public UserRatingDto rateUser(@PathVariable Long id, @PathVariable Integer value) {
    return service.rateUser(id, value);
  }

  @GetMapping("/api/stats/users/sales-unit")
  public List<UserSalesStatsDto> userSalesUnitStats(
      @RequestParam(defaultValue = "desc") String order) {
    return service.salesUnitStats(order);
  }

  @GetMapping("/api/stats/users/sales-value")
  public List<UserSalesStatsDto> userSalesValueStats(
      @RequestParam(defaultValue = "desc") String order) {
    return service.salesValueStats(order);
  }

  @GetMapping("/api/stats/users/sales-value/top5")
  public List<UserSalesStatsDto> userSalesValueStatsTop5(
      @RequestParam(defaultValue = "desc") String order) {
    return service.salesValueStats(order).stream().limit(5).collect(Collectors.toList());
  }

  @GetMapping("/api/stats/users/rating")
  public List<UserRatingDto> listUserByRating(@RequestParam(defaultValue = "desc") String order) {
    return service.listUserByRating(order);
  }

  //  GET		/api/stats/users/sales-value/top5
  //  @GetMapping("/api/stats/users/sales-value/top5")
}
