package com.ikrisztian.marketplace.controller;

import com.ikrisztian.marketplace.dto.out.ErrorDto;
import com.ikrisztian.marketplace.exception.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class MarketPlaceControllerAdvice {

  private static final Logger LOGGER = LoggerFactory.getLogger(MarketPlaceControllerAdvice.class);

  @ResponseStatus(HttpStatus.BAD_REQUEST)
  @ExceptionHandler(DataIntegrityViolationException.class)
  @ResponseBody
  public ErrorDto handleDuplicateEmailAddresses(DataIntegrityViolationException exception) {
    return handleException(exception, "Email already in use", 1);
  }

  @ResponseStatus(HttpStatus.NOT_FOUND)
  @ExceptionHandler(UserNotFoundException.class)
  @ResponseBody
  public ErrorDto handleUserNotFoundException(MarketPlaceException exception) {
    return handleMarketPlaceException(exception);
  }

  @ResponseStatus(HttpStatus.NOT_FOUND)
  @ExceptionHandler(ProductNotFoundException.class)
  @ResponseBody
  public ErrorDto handleProductNotFoundException(MarketPlaceException exception) {
    return handleMarketPlaceException(exception);
  }

  @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
  @ExceptionHandler(ProductOutOfStockException.class)
  @ResponseBody
  public ErrorDto handleProductOutOfStockException(MarketPlaceException exception) {
    return handleMarketPlaceException(exception);
  }

  @ResponseStatus(HttpStatus.BAD_REQUEST)
  @ExceptionHandler(IllegalRatingValueException.class)
  @ResponseBody
  public ErrorDto handleIllegalRatingValueException(MarketPlaceException exception) {
    return handleMarketPlaceException(exception);
  }

  private ErrorDto handleMarketPlaceException(MarketPlaceException exception) {
    return handleException(exception, exception.getMessage(), exception.getErrorCode());
  }

  private ErrorDto handleException(Exception exception, String message, int errorCode) {
    LOGGER.debug(message, exception);
    ErrorDto errorDto = new ErrorDto();
    errorDto.setErrorCode(errorCode);
    errorDto.setMessage(message);
    errorDto.setDescription(message);
    return errorDto;
  }
}
