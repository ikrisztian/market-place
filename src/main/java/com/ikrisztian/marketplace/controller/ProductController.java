package com.ikrisztian.marketplace.controller;

import com.ikrisztian.marketplace.dto.in.EditProductDto;
import com.ikrisztian.marketplace.dto.out.ProductCategorySalesStatsDto;
import com.ikrisztian.marketplace.dto.out.ProductDto;
import com.ikrisztian.marketplace.dto.out.ProductPopularityDto;
import com.ikrisztian.marketplace.dto.out.ProductSalesStatsDto;
import com.ikrisztian.marketplace.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
public class ProductController {
  private ProductService service;

  @Autowired
  public ProductController(ProductService service) {
    this.service = service;
  }

  @ResponseStatus(HttpStatus.CREATED)
  @PostMapping("/api/products")
  public ProductDto save(@Valid @RequestBody EditProductDto editProductDto) {
    return service.save(editProductDto);
  }

  @GetMapping("/api/products")
  public List<ProductDto> listProducts() {
    return service.listProducts();
  }

  @GetMapping("/api/products/{id}")
  public ProductDto listProduct(@PathVariable Long id) {
    return service.listProductById(id);
  }

  @PutMapping("/api/products/{id}")
  public ProductDto updateProduct(
      @PathVariable Long id, @Valid @RequestBody EditProductDto editProductDto) {
    return service.updateProduct(id, editProductDto);
  }

  @DeleteMapping("/api/products/{id}")
  @ResponseStatus(HttpStatus.NO_CONTENT)
  public void deleteProduct(@PathVariable Long id) {
    service.delete(id);
  }

  @PostMapping("/api/products/{id}/buy")
  public ProductDto buyProduct(@PathVariable Long id) {
    return service.buy(id);
  }

  @GetMapping("/api/products/{id}/popularity")
  public ProductPopularityDto popularity(@PathVariable Long id) {
    return service.popularityForId(id);
  }

  @GetMapping("/api/stats/products/{id}")
  public ProductSalesStatsDto saleStats(@PathVariable Long id) {
    return service.saleStats(id);
  }

  @GetMapping("/api/stats/products/sales-stats")
  public List<ProductSalesStatsDto> salesStats(@RequestParam(defaultValue = "asc") String order) {
    return service.salesStats(order);
  }

  @GetMapping("/api/stats/products/views/top5")
  public List<ProductPopularityDto> productPopularityDtosTop5(
      @RequestParam(defaultValue = "desc") String order) {
    return service.popularityTop5();
  }

  @GetMapping("/api/stats/categories")
  public List<ProductCategorySalesStatsDto> listProductCategorySalesStats() {
    return service.getProductCategorySalesStats();
  }
}
