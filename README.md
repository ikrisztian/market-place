You can find the API here:
https://documenter.getpostman.com/view/6282556/RznCpeaV#148453a0-861e-4e70-98de-8f9d293c7a88

The swagger interface of the API can be found here:
http://localhost:8080/swagger-ui.html